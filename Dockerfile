# python stuff
FROM python:3.7-stretch AS pyscripts
RUN mkdir /install
WORKDIR /install
COPY requirements.txt /requirements.txt

RUN pip install -r /requirements.txt

# grab the rtlamr go app
FROM golang:stretch AS rtlamr
RUN go get github.com/bemasher/rtlamr

FROM node:8

# Libraries needed for various nodes
RUN apt-get update && apt-get install -y \
    bluetooth \
    libglib2.0-dev \
    libbluetooth-dev \
    apcupsd \
    python-pip \
    # for SpeedTest
    build-essential \
    libcurl4-openssl-dev \
    libxml2-dev \
    libssl-dev \
    cmake \
    git
RUN pip install --upgrade pip \
    speedtest-cli

# SpeedTest
RUN git clone https://github.com/taganaka/SpeedTest && \
    cd SpeedTest && \
    cmake -DCMAKE_BUILD_TYPE=Release . && \
    make install

# Agent smith setup
RUN mkdir /scripts
COPY scripts/bamm_deals.py /scripts
COPY --from=pyscripts /install /usr/local

# rtlamr
COPY --from=rtlamr /go /go

# Set up node-red
RUN mkdir /data
RUN mkdir -p /opt/node-red
WORKDIR /opt/node-red
COPY package.json /opt/node-red
RUN npm install
EXPOSE 1880
ENV FLOWS=flows.json
ENV NODE_PATH=/opt/node-red/node_modules:/data/node_modules

# bluetooth dongle firmware
COPY ./BCM20702A1-0a5c-21ec.hcd /lib/firmware/BCM20702A1-0a5c-21ec.hcd

# Install various Node-RED nodes
RUN cd /opt/node-red/node_modules && \
    npm install node-red-contrib-noble && \
    npm install node-red-contrib-postgres-variable && \
    npm install node-red-contrib-apcaccess && \
    npm install node-red-node-daemon && \
    npm install node-red-node-pushbullet && \
    npm install node-red-contrib-nora && \
    git clone https://github.com/impala454/node-red.git ruuvi-node && \
    cd ruuvi-node && \
    npm link && \
    cd .. && \
    git clone https://github.com/impala454/node-red-contrib-tuya-local.git && \
    cd node-red-contrib-tuya-local && \
    npm link && \
    cd ..

CMD ["npm", "start", "--", "--userDir", "/data"]
