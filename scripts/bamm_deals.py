#!/usr/bin/env python

import datetime
import json
import sys
import urllib.request

page = urllib.request.urlopen('https://www.facebook.com/Bay-Area-Meat-Market-Deli-159704177386461/')
contents = page.read().decode(page.headers.get_content_charset())
lower_contents = contents.lower()

search = ['ground chuck', 'brisket', 'ribs']

for text in search:
    if text in lower_contents:
        m_index = lower_contents.find(text)
        p_index = contents.rfind('<p>', 0, m_index)
        p_end_index = contents.find('</p>', p_index)
        msg = contents[p_index+3:p_end_index].replace('<br />', '\n')
        d_utime_index = contents.rfind('data-utime="', 0, p_index)
        date_str = contents[d_utime_index+12:d_utime_index+22]
        date = datetime.datetime.fromtimestamp(int(date_str))
        ret = {"date": date.strftime("%Y-%m-%d %H:%M:%S"), "msg": msg}
        print(json.dumps(ret))
        sys.exit(0)
    else:
        ret = {}
        print(json.dumps(ret))
        sys.exit(0)
