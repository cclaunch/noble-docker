Noble Docker
============

This docker image is loosely based on the node-red-docker by adding the [Noble](https://flows.nodered.org/node/node-red-contrib-noble) node, used to interact with Blueooth Low Energy (BLE) devices.  It also adds the [Ruuvi Node](https://github.com/ojousima/node-red) which easily parses data from [Ruuvi Tags](https://ruuvi.com/).  Other catch-all nodes may be added which don't necessarily pertain only to this purpose though.

*Note* The node-red-docker this was based on creates a `node-red` user to run with.  I have not found a way to run the docker as anything but root and still successfully pass in the USB BLE device.  Therefore this container runs as `root`.

Pull
----

The build for this docker pushes to the docker hub registry `impala454/noble-docker` and may be pulled with that repository.

Run
---

To run this container you'll need to add a path variable with the path to your BLE device.  First find the BLE device by executing

```
lsusb
```

Noting the bus and device IDs, replace them in the following path: `/dev/bus/usb/00X/00X`.  Note that this path is used to avoid unnecessarily setting up the device's firmware on the docker host.  Now you may run the container similar to the normal node-red-docker but with this new path added

```
docker run -it -p 1880:1880 -v /dev/bus/usb/00X/00X:/dev/bus/usb/00X/00X --name mynodered
```
